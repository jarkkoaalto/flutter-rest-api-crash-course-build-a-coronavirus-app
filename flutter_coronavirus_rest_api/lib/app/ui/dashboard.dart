import 'package:flutter/material.dart';
import 'package:flutter_coronavirus_rest_api/app/repository/data_repository.dart';
import 'package:flutter_coronavirus_rest_api/app/repository/endpoints_data.dart';
import 'package:flutter_coronavirus_rest_api/app/services/api.dart';
import 'package:flutter_coronavirus_rest_api/app/ui/endpoint_card.dart';
import 'package:flutter_coronavirus_rest_api/app/ui/last_updated_status.dart';
import 'package:provider/provider.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  EndpointsData _endpointsData;

  @override
  void initState() {
    super.initState();
    _updateData();
  }

  Future<void> _updateData() async {
    final dataRepository = Provider.of<DataRepository>(context, listen: false);
    final endpointsData = await dataRepository.getAllEndpointsData();
    setState(() => _endpointsData = endpointsData);
  }

  @override
  Widget build(BuildContext context) {
    final formatter = LastUpdatedDateFormatter(
      lastUpdated: _endpointsData != null
          ? _endpointsData.values[Endpoint.cases].date
          : null,
    );
    return Scaffold(
      appBar: AppBar(
        title: Text('Coronavirus Tracker'),
      ),
      body: RefreshIndicator(
        onRefresh: _updateData,
        child: ListView(
          children: <Widget>[
            LastUpdatedStatusText(
              text: formatter.lastUpdatedStatusText(),
              // text: _endpointsData != null
              // We do not run for loop. We can choose cases, because all endpoint values contain a date information
              //     ? _endpointsData.values[Endpoint.cases].date?.toString() ?? ''
              //     : '',
            ),
            for (var endpoint in Endpoint.values)
              EndpointCard(
                endpoint: endpoint,
                value: _endpointsData != null
                    ? _endpointsData.values[endpoint].value
                    : null,
              ),
          ],
        ),
      ),
    );
  }
}
