
# flutter_coronavirus_rest_api

![Screenshot](app.png)

## Useful Links and resources

- GitHub Public APIs repository: https://github.com/public-apis/public-apis
- REST API tutorial: https://restfulapi.net/
- nCov 2019 API Dashboard: https://ncov2019-admin.firebaseapp.com/#/
- REST Client extension for Visual Studio Code: https://marketplace.visualstudio.com/items?itemName=humao.rest-client&ssr=false#overview
- Error Lens: visual studio code extensions: https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens
- Starter Architecture for Flutter & Firebase Apps | Code With Andrea: https://codewithandrea.com/videos/2020-02-10-starter-architecture-flutter-firebase/
- Dart http package: https://pub.dev/packages/http
- Source code for this course on GitHub
- Uniform Resource Identifier | Wikipedia: https://en.wikipedia.org/wiki/Uniform_Resource_Identifier
- Dart Library Tour | URIs: https://dart.dev/guides/libraries/library-tour#uris
- Dart Futures | Flutter in Focus
- Async / Await | Flutter in Focus
- Asynchronous programming: futures, async, await (Codelab): https://dart.dev/codelabs/async-await
- Futures and Error Handling: https://dart.dev/guides/libraries/futures-error-handling
- json_serializable | Dart Build System builders for handling JSON: https://pub.dev/packages/json_serializable
- json_annotation | Defines the annotations used by json_serializable to create code for JSON serialization and deserialization: https://pub.dev/packages/json_annotation
- Code With Andrea | YouTube: https://www.youtube.com/c/CodeWithAndrea
- Provider package | pub.dev: https://pub.dev/packages/provider
- Pubspec Assist extension for VS Code: https://marketplace.visualstudio.com/items?itemName=jeroen-meijer.pubspec-assist
- TextTheme class: https://api.flutter.dev/flutter/material/TextTheme-class.html
- Flutter Layouts Walkthrough: Row, Column, Stack, Expanded, Padding: https://www.youtube.com/watch?v=RJEnTRBxaSg&feature=youtu.be
- Provider YouTube Playlist | Code With Andrea: https://www.youtube.com/playlist?list=PLNnAcB93JKV-IarNvMKJv85nmr5nyZis8
- Dart Futures - Flutter in Focus: https://www.youtube.com/watch?v=OTS-ap9_aXc&feature=youtu.be
- Async/Await - Flutter in Focus: https://www.youtube.com/watch?v=SmTCmDMi4BY&feature=youtu.be
- Future.wait method: https://api.dart.dev/stable/2.7.1/dart-async/Future/wait.html
- Generics | Dart Language Tour: https://dart.dev/guides/language/language-tour#generics
- Dart Features For Better Code: Spreads, Collection-If, Collection-For: https://www.youtube.com/watch?v=mnaN_6465Gk&feature=youtu.be
- How do I use hexadecimal color strings in Flutter?: https://stackoverflow.com/questions/50081213/how-do-i-use-hexadecimal-color-strings-in-flutter
- Flutter Layouts Walkthrough: Row, Column, Stack, Expanded, Padding: https://www.youtube.com/watch?v=RJEnTRBxaSg&feature=youtu.be



App Architecture:

|      Architecture                                         |    Where                               |
| --------------------------------------------------------- | -------------------------------------- |
|  nCoV 2019 Rest Api                                       |       Outside world                    |
|  APIService - token and endpoint data                     |       Domain Layer                     |
|  DataRepository                                           |       Presentation & logic Layer       |
|  Dashboard                                                |       UI Layer                         |

ApiService class:
- Pure (holds no state)
- but we need state to hold the access token, so that we can call the remaining endpoint -> DataRepository


API class:

|  Make request / parse responses |       |  List all endpoints         |        |  Holds API keys  |
| ------------------------------- | ----- | --------------------------- | ------ | ---------------- |
| API Services                    | uses  | API                         | uses   | API Keys         |
| token / endpoint data           |       |   sandbox / production      |        |                  |


### API Service ###
- Abstract away implementation details (make requests, parse JSON)
- Expose a domain-specific API


### Let's talk about design ###

#### Refresh UX options ####
- Pull to refresh
- Refresh Button

#### Loading and refreshing data ####
- We will use pull to refresh
- But feel free to try something else

#### Combine async data

List<Future>:
- cases
- suspected cases
- confirmed cases
- deaths
- recovered

to combine async data

Future<List>
- data

- when we used await Future.wait([]). All futures execute in parallel (concurrently).
- When all futures have completed, result is returned.

- This returns a List<int>.
- All endpoints data can be represented as Map<Endpoint, int>